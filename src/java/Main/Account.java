package Main;

public class Account implements Comparable{

    private Long accountNumber; // - номер счета
    private Double balance;     // - баланс

    public static long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;

    public Account(Client client, Double balance) {
        this.accountNumber = (Long) GLOBAL_ACCOUNT_NUMBER;
        this.balance = balance;
        GLOBAL_ACCOUNT_NUMBER++;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return accountNumber.equals(account.accountNumber);
    }

    @Override
    public int hashCode() {
        return accountNumber.hashCode();
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                '}';
    }

    public void increaseBalance(Double sum) {
        this.balance += sum;
    }

    public void decreaseBalance(Double sum) {
        this.balance -= sum;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
