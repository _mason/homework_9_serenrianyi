package Main;

import enums.Operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Bank {

    private List<Client> clients = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();

    public void addAccount(Client client) {
        Account account = new Account(client, (double) 0);
        accounts.add(client.addAccount(account));
    }

    public boolean addClient(Client client) {
        if (findClient(client) == null) {
            clients.add(client);
            return true;
        } else {
            System.out.println("Client is in base");
            return false;
        }
    }

    public Client findClient(Client searchClient) {
        if (clients.contains(searchClient)) return searchClient;
        return null;
    }

    public boolean putIntoAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if ((account != null) && (amount > 0)) {
            Transaction transaction = new Transaction(account, Operation.PUT, amount);
            transactions.add(transaction);
            account.increaseBalance(amount);
            return true;
        } else
            System.out.println("it is impossible to perform the operation");
        return false;
    }

    public Account findAccount(long searchAccountNumber) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getAccountNumber().equals(searchAccountNumber)) return accounts.get(i);
        }
        return null;
    }

    public boolean withdrawFromAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if ((account != null) && (amount > 0) && (account.getBalance() > amount)) {
            Transaction transaction = new Transaction(account, Operation.WITHDRAW, amount);
            transactions.add(transaction);
            account.decreaseBalance(amount);
            return true;
        } else {
            System.out.println("it is impossible to perform the operation");
            return false;
        }
    }

    public boolean moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount) {
        if (findAccount(sourceAccountNumber) != null && findAccount(destinationAccountNumber) != null) {
            withdrawFromAccount(sourceAccountNumber, amount);
            putIntoAccount(destinationAccountNumber, amount);
            return true;
        } else {
            System.out.println("it is impossible to perform the operation");
            return false;
        }
    }

    public List<Account> findAccountsByClient(Client searchClient) {
        for (int i = 0; i < clients.size(); i++) {
            if (clients.get(i).equals(searchClient)) return clients.get(i).getAccounts();
        }
        return null;
    }

    public void printClients() {
        for (int i = 0; i < clients.size(); i++) {
            System.out.println(clients.get(i).toString());
        }
    }

    public void printAccounts() {
        Collections.sort(accounts);
        for (Account str : accounts) {
            System.out.print(" " + str);
        }
    }

    public void printAccountsForClient(Client client) {
        for (int i = 0; i < accounts.size(); i++) {
            if (clients.get(i).equals(client)) System.out.println(clients.get(i).toString());
        }
    }

    public void printTransactions() {
        for (int i = 0; i < transactions.size(); i++) {
            System.out.println(transactions.get(i).toString());
        }
    }
    
    public List<Transaction> findTransactionsByAccount(long searchAccountNumber) {
        List<Transaction> result = new ArrayList<>();
        for (int i = 0; i < transactions.size(); i++) {
            if (transactions.get(i).getSource().equals(searchAccountNumber)) {
                result.add(transactions.get(i));
                if (transactions == null) return null;
            }
        }
        return result;
    }

    public void printTransactionsForAccount(Long accountNumber) {
        List<Transaction> result = findTransactionsByAccount(accountNumber);
        for (Transaction t : result) {
            System.out.println(t.toString());
        }
    }

}
